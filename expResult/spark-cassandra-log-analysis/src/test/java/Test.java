import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hxd on 17/2/17.
 */
public class Test {
    public static void main(String[] args){
        Pattern pattern=Pattern.compile("(s\\d+)?/(\\d+.\\d+.\\d+.\\d+)");
        Matcher matcher=pattern.matcher("/172.30.0.132/172.30.0.142");
        while(matcher.find()) {
            System.out.println(matcher.group(2));
        }
    }
}
