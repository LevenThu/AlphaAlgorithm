//package cn.edu.thu.process;
//
//import cn.edu.thu.common.Utils;
//import cn.edu.thu.model.Event;
//import cn.edu.thu.model.PresSucs;
//import cn.edu.thu.model.Trace;
//
//import java.io.IOException;
//import java.util.*;
//import java.util.concurrent.ForkJoinPool;
//import java.util.concurrent.RecursiveAction;
//
//public class Miner {
//
//    private static int count = 0;
//
//    private Collection<Trace> traces;
//    private Map<Event, Set<Event>> positiveMap;
//    private Map<Event, Set<Event>> reverseMap;
//    private Set<Trace> parallels;
//
//    public Miner(Collection<Trace> traces) {
//        this.traces = traces;
//        this.positiveMap = new HashMap<>();
//        this.reverseMap = new HashMap<>();
//        this.parallels = new HashSet<>();
//    }
//
//    public void mine() {
//        pre_process();
//        process();
//    }
//
//    private void pre_process() {
//        traces.stream().filter(trace -> trace.size() > 1).forEach(trace -> {
//            for (int i = 0; i < trace.size() - 1; ++i) {
//                Event precursor = trace.get(i);
//                Event succeed = trace.get(i + 1);
//
//                Set<Event> succeeds = positiveMap.computeIfAbsent(precursor, obj -> new HashSet<>());
//                succeeds.add(succeed);
//                positiveMap.put(precursor, succeeds);
//
//                Set<Event> precursors = reverseMap.computeIfAbsent(succeed, obj -> new HashSet<>());
//                precursors.add(precursor);
//                reverseMap.put(succeed, precursors);
//            }
//        });
//
//        positiveMap.forEach((precursor, succeeds) ->
//            succeeds.forEach(succeed -> {
//                if (positiveMap.containsKey(succeed) && positiveMap.get(succeed).contains(precursor)) {
//                    parallels.add(new Trace(precursor, succeed));
//                }
//            })
//        );
//
//        parallels.forEach(parallel -> {
//            Event precursor = parallel.get(0);
//            Event succeed = parallel.get(1);
//
//            positiveMap.computeIfAbsent(precursor, obj -> new HashSet<>()).remove(succeed);
//            if (positiveMap.get(precursor).size() == 0) positiveMap.remove(precursor);
//            positiveMap.computeIfAbsent(succeed, obj -> new HashSet<>()).remove(precursor);
//            if (positiveMap.get(succeed).size() == 0) positiveMap.remove(succeed);
//
//            reverseMap.computeIfAbsent(precursor, obj -> new HashSet<>()).remove(succeed);
//            if (reverseMap.get(precursor).size() == 0) reverseMap.remove(precursor);
//            reverseMap.computeIfAbsent(succeed, obj -> new HashSet<>()).remove(precursor);
//            if (reverseMap.get(succeed).size() == 0) reverseMap.remove(succeed);
//        });
//    }
//
//    private Set<PresSucs> process() {
//        Set<PresSucs> result = new HashSet<>();
//        ForkJoinPool pool = new ForkJoinPool();
//
//        positiveMap.forEach((precursor, succeeds) -> pool.invoke(new SplitDC(result, new PresSucs(precursor, succeeds), true)));
//
//        Map<Set<Event>, Set<Event>> temp = new HashMap<>();
//        result.forEach(presSucs -> {
//            Set<Event> succeeds = presSucs.succeeds;
//            Set<Event> precursors = presSucs.precursors;
//            Set<Event> allPrecursors = temp.computeIfAbsent(succeeds, obj -> new HashSet<>());
//            allPrecursors.addAll(precursors);
//        });
//        result.clear();
//
//        temp.forEach((succeeds, precursors) -> pool.invoke(new SplitDC(result, new PresSucs(precursors, succeeds), true)));
//        return result;
//    }
//
//    private class SplitDC extends RecursiveAction {
//
//        private Set<PresSucs> result;
//        private PresSucs presSucs;
//        private boolean isSucs;
//
//        public SplitDC(Set<PresSucs> result, PresSucs presSucs, boolean isSucs) {
//            this.result = result;
//            this.presSucs = presSucs;
//            this.isSucs = isSucs;
//        }
//
//        @Override
//        protected void compute() {
//            Set<Event> noSplit = isSucs ? presSucs.precursors : presSucs.succeeds;
//            Set<Event> toSplit = isSucs ? presSucs.succeeds : presSucs.precursors;
//            if (toSplit.size() <= 1) {
//                result.add(presSucs);
//                return;
//            }
//
//            Event splitEventA = null;
//            Event splitEventB = null;
//            for (Event fellow : toSplit) {
//                Trace parallel = getParallel(toSplit);
//                if (null != parallel) {
//                    splitEventA = parallel.get(0);
//                    splitEventB = parallel.get(1);
//                    break;
//                }
//
//                splitEventB = getRelative(toSplit, positiveMap.get(fellow));
//                if (null != splitEventB) {
//                    splitEventA = fellow;
//                    break;
//                }
//
//                splitEventB = getRelative(toSplit, reverseMap.get(fellow));
//                if (null != splitEventB) {
//                    splitEventA = fellow;
//                    break;
//                }
//            }
//
//            if (null == splitEventB) {
//                result.add(presSucs);
//                return;
//            }
//
//            toSplit.remove(splitEventA);
//            Set<Event> subsetA = new HashSet<>(toSplit);
//            PresSucs partA = isSucs ? new PresSucs(noSplit, subsetA) : new PresSucs(subsetA, noSplit);
//            invokeAll(new SplitDC(result, partA, isSucs));
//            toSplit.add(splitEventA);
//
//            toSplit.remove(splitEventB);
//            Set<Event> subsetB = new HashSet<>(toSplit);
//            PresSucs partB = isSucs ? new PresSucs(noSplit, subsetB) : new PresSucs(subsetB, noSplit);
//            invokeAll(new SplitDC(result, partB, isSucs));
//        }
//    }
//
//
//    private Trace getParallel(Set<Event> fellows) {
//        for (Event fellow : fellows) {
//            for (Trace trace : parallels) {
//                if (trace.get(0).equals(fellow) && fellows.contains(trace.get(1)))
//                    return trace;
//
//                if (trace.get(1).equals(fellow) && fellows.contains(trace.get(0)))
//                    return trace;
//            }
//        }
//
//        return null;
//    }
//
//    private Event getRelative(Set<Event> fellows, Set<Event> relatives) {
//        if (null == relatives)
//            return null;
//
//        for (Event relative : relatives) {
//            if (fellows.contains(relative)) {
//                return relative;
//            }
//        }
//
//        return null;
//    }
//
//
//    public static void main(String[] args) throws IOException {
////        List<Trace> traces = new ArrayList<>();
////        traces.add(new Trace("a", "b", "c", "d"));
////        traces.add(new Trace("a", "c", "b", "d"));
////        traces.add(new Trace("a", "e", "d"));
//
//        long start = System.currentTimeMillis();
//        String fileName = "4n_100tps_10threads_write_event.xes";
//        Set<Trace> traces = Utils.parseXESFile(fileName);
//        Miner miner = new Miner(traces);
//        miner.mine();
//        long end = System.currentTimeMillis();
//        System.out.println("multi thread : " + (end - start));
//    }
//
//}
